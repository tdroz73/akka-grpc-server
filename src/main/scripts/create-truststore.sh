#!/bin/bash

export PW=`cat ~/password`

# Create a JKS keystore that trusts the example CA, with the default password.
keytool -import -v \
  -alias xymoxLocalCA \
  -file xymoxCA.crt \
  -keypass $PW \
  -storepass changeit \
  -keystore xymoxTrust.jks << EOF
yes
EOF

# List out the details of the store password.
keytool -list -v \
  -keystore xymoxTrust.jks \
  -storepass changeit