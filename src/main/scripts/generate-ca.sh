#!/usr/bin/env bash

export PW=`cat ~/password`

# Create a self signed key pair root CA certificate.
keytool -genkeypair -v \
  -alias xymoxLocalCA \
  -dname "CN=xymoxLocalCA, OU=Xymox.net, O=Xymox Networks, L=Phoenix, ST=AZ, C=US" \
  -keystore xymoxCA.jks \
  -keypass $PW \
  -storepass $PW \
  -keyalg RSA \
  -keysize 4096 \
  -ext KeyUsage:critical="keyCertSign" \
  -ext BasicConstraints:critical="ca:true" \
  -validity 9999

# Export the exampleCA public certificate as exampleca.crt so that it can be used in trust stores.
keytool -export -v \
  -alias xymoxLocalCA \
  -file xymoxCA.crt \
  -keypass $PW \
  -storepass $PW \
  -keystore xymoxCA.jks \
  -rfc
