#!/usr/bin/env bash

export PW=`cat ~/password`

# Create a server certificate, tied to example.com
keytool -genkeypair -v \
  -alias xymox.net \
  -dname "CN=localhost.xymox.net, OU=Xymox Networks, O=Xymox Networks, L=Phoenix, ST=Arizona, C=US" \
  -keystore xymox.net.jks \
  -keypass $PW \
  -storepass $PW \
  -keyalg RSA \
  -keysize 2048 \
  -validity 385

# Create a certificate signing request for example.com
keytool -certreq -v \
  -alias xymox.net \
  -keypass $PW \
  -storepass $PW \
  -keystore xymox.net.jks \
  -file xymox.net.csr

# Tell exampleCA to sign the example.com certificate. Note the extension is on the request, not the
# original certificate.
# Technically, keyUsage should be digitalSignature for DHE or ECDHE, keyEncipherment for RSA.
keytool -gencert -v \
  -alias xymoxLocalCA \
  -keypass $PW \
  -storepass $PW \
  -keystore xymoxCA.jks \
  -infile xymox.net.csr \
  -outfile xymox.net.crt \
  -ext KeyUsage:critical="digitalSignature,keyEncipherment" \
  -ext EKU="serverAuth" \
  -ext SAN="DNS:localhost.xymox.net" \
  -rfc

# Tell example.com.jks it can trust exampleca as a signer.
keytool -import -v \
  -alias xymoxLocalCA \
  -file xymoxCA.crt \
  -keystore xymox.net.jks \
  -storetype JKS \
  -storepass $PW << EOF
yes
EOF

# Import the signed certificate back into example.com.jks 
keytool -import -v \
  -alias xymox.net \
  -file xymox.net.crt \
  -keystore xymox.net.jks \
  -storetype JKS \
  -storepass $PW

# List out the contents of example.com.jks just to confirm it.  
# If you are using Play as a TLS termination point, this is the key store you should present as the server.
keytool -list -v \
  -keystore xymox.net.jks \
  -storepass $PW