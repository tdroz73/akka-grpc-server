export PW=`cat ~/password`

# Export xymox.net's public certificate for use with nginx.
keytool -export -v \
  -alias xymox.net \
  -file xymox.net.crt \
  -keypass $PW \
  -storepass $PW \
  -keystore xymox.net.jks \
  -rfc

# Create a PKCS#12 keystore containing the public and private keys.
keytool -importkeystore -v \
  -srcalias xymox.net \
  -srckeystore xymox.net.jks \
  -srcstoretype jks \
  -srcstorepass $PW \
  -destkeystore xymox.net.p12 \
  -destkeypass $PW \
  -deststorepass $PW \
  -deststoretype PKCS12