package net.xymox.example.grpc

import java.io.InputStream
import java.security.{KeyStore, SecureRandom}

import akka.actor.ActorSystem
import akka.http.scaladsl.{ConnectionContext, Http2, HttpConnectionContext, HttpsConnectionContext}
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.{ActorMaterializer, Materializer}
import javax.net.ssl.{KeyManagerFactory, SSLContext, TrustManagerFactory}

import scala.concurrent.{ExecutionContext, Future}

object Main extends App {
  val system = ActorSystem("HelloGreeter")
  new GreeterServer(system).run()
}

class GreeterServer(system : ActorSystem) {
  def run() = {
    implicit val sys: ActorSystem = system
    implicit val mat: Materializer = ActorMaterializer()
    implicit val ec: ExecutionContext = sys.dispatcher

    val service : HttpRequest => Future[HttpResponse] = GreeterServiceHandler(new GreeterServiceImpl())

    val binding = Http2().bindAndHandleAsync(
      service,
      interface = "127.0.0.1",
      port = 8080,
      connectionContext = createConnectionContext)

    binding.foreach { binding =>
      println(s"gRPC server bound to: ${binding.localAddress}")
    }
    binding
  }

  private def createConnectionContext: HttpsConnectionContext = {
    // Manual HTTPS configuration
    val password: Array[Char] = "fnR4vcW3rd".toCharArray // do not store passwords in code, read them from somewhere safe!

    val ks: KeyStore = KeyStore.getInstance("jks")
    val keystore: InputStream = getClass.getClassLoader.getResourceAsStream("xymox.net.jks")

    require(keystore != null, "Keystore required!")
    ks.load(keystore, password)

    val keyManagerFactory: KeyManagerFactory = KeyManagerFactory.getInstance("SunX509")
    keyManagerFactory.init(ks, password)

    val tmf: TrustManagerFactory = TrustManagerFactory.getInstance("SunX509")
    tmf.init(ks)

    val sslContext: SSLContext = SSLContext.getInstance("TLS")
    sslContext.init(keyManagerFactory.getKeyManagers, tmf.getTrustManagers, new SecureRandom)
    ConnectionContext.https(sslContext)
  }
}
