package net.xymox.example.grpc
import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}

import scala.concurrent.Future

class GreeterServiceImpl(implicit mat : Materializer) extends GreeterService {
  import mat.executionContext
  /**
   * ////////////////////
   * Sends a greeting //
   * //////&#42;****&#47;////////
   * HELLO       //
   * //////&#42;****&#47;////////
   */
  override def sayHello(in: HelloRequest): Future[HelloReply] = {
    println(s"sayHello to ${in.name}")
    Future.successful(HelloReply(s"Hello, ${in.name}"))
  }

  /**
   * Comment spanning
   * on several lines
   */
  override def itKeepsTalking(in: Source[HelloRequest, NotUsed]): Future[HelloReply] = {
    println(s"sayHello to in stream...")
    in.runWith(Sink.seq).map(elements => HelloReply(s"Hello, ${elements.map(_.name).mkString(", ")}"))
  }

  /**
   * C style comments
   */
  override def itKeepsReplying(in: HelloRequest): Source[HelloReply, NotUsed] = {
    println(s"sayHello to ${in.name} with stream of chars...")
    Source(s"Hello, ${in.name}".toList).map(character => HelloReply(character.toString))
  }

  /**
   * C style comments
   * on several lines
   * with non-empty heading/trailing line    */
  override def streamHellos(in: Source[HelloRequest, NotUsed]): Source[HelloReply, NotUsed] = {
    println(s"sayHello to stream...")
    in.map(request => HelloReply(s"Hello, ${request.name}"))
  }
}
