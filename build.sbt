organization := "net.xymox"

name := "akka-grpc-server"

version := "0.1"

scalaVersion := "2.13.0"

enablePlugins(AkkaGrpcPlugin)

